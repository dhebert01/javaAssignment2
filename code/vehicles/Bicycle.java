//David Hebert 2141781
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufactuerer, int numberGears, double maxSpeed){
        this.manufacturer = manufactuerer;
        this.maxSpeed = maxSpeed;
        this.numberGears = numberGears;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufactuerer: " + this.manufacturer + ", Number of gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}
