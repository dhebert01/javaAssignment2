//David Hebert 2141781
package application;
import vehicles.Bicycle;

public class BikeStore{
    public static void main(String[] args){
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Avigo", 5, 20);
        bicycles[1] = new Bicycle("Trek", 8, 25);
        bicycles[2] = new Bicycle("Canyon", 7, 23);
        bicycles[3] = new Bicycle("Bianchi", 8, 26);

        for (int i= 0; i < bicycles.length; i++){
            System.out.println(bicycles[i]);
        }
        
    }
}
